﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Chat.Test
{
    [TestFixture]
    public class ConnectorShould
    {
        [Test]
        public void SendMessage()
        {
            var client = new {LastMessage = ""};
            var sendedMessage = client.LastMessage;
            Assert.AreEqual("Client connected", sendedMessage);
        }
    }
}
