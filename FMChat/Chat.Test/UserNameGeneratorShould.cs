﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Chat.Test
{
    [TestFixture]
    class UserNameGeneratorShould
    {
        [Test]
        public void GenerateNotNullUserName()
        {
            UserNameGenerator userNameGenerator = new UserNameGenerator();

            var userName = userNameGenerator.GenerateUserName();

            Assert.That(userName, Is.Not.Null);
        }

        //[Test]
        //public void Generate
    }

    internal class UserNameGenerator
    {
        internal string GenerateUserName()
        {
            return "123";
        }
    }
}